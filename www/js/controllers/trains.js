angular.module('starter')
.controller('TrainsCtrl', function($scope, $timeout, TrainApi) {
  $scope.trains = [];
  
  $scope.onTrainDelete = function(train, index){
    console.log(train);
    TrainApi.delete({id: train.id}, function(response){
      $scope.trains.splice(index, 1);
      console.log(response);
    }, function(error){
      console.log(error);
    })
  }

  var init = function(){
    TrainApi.query(function(response){
      $scope.trains = response;
    }, function(error){
      alert(JSON.stringify(error));
    })
  }

  $scope.$on('$ionicView.enter', function(e) {
    //init();
  });

  $timeout(function(){
    init();
  }, 300);
})

.controller('TrainsNewCtrl', function($scope, $state, $ionicPopup, TrainApi) {
  $scope.train = {};

  var showAlert = function(){
    var alertPopup = $ionicPopup.alert({
      title: 'Tren creado',
      template: 'Todo salió Joyita!'
    });
    alertPopup.then(function(res) {
      console.log('Thank you for not eating my delicious ice cream cone');
      $state.go('app.trains');
    });
  }

  $scope.add = function(trainParam){
    var data = {
      number: trainParam.number,
      name: trainParam.name,
      state: trainParam.state
    }
    TrainApi.save(data, function(response){
      console.log(response);
      showAlert();
    }, function(error){
      console.log(error);
    })
  }


})

.controller('TrainsShowCtrl', function($scope, $stateParams, TrainApi) {
  console.log("TrainsShowCtrl", $stateParams.trainId);
  
  $scope.$on('$ionicView.enter', function(e) {
    TrainApi.get({id: $stateParams.trainId}, function(response){
      $scope.train = response;
    }, function(error){
      console.log(error);
    })
  });
})