// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'ngResource'])

.run(function($ionicPlatform, TrainDB) {

  var initSQLite = function(){
    TrainDB.init(); 
    TrainDB.all().then(function(resolve){
      console.log(resolve);
    }, function(reject){
      console.log(reject);
    });  
    var tren = {
      name: "Tren",
      number: Math.floor(Math.random() * 600) + 1,
      state: "activo"
    }
    TrainDB.create(tren).then(function(resolve){
      console.log(resolve);
    }, function(reject){
      console.log(reject);
    });
  }

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    initSQLite();
  });
});
