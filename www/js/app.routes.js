angular.module('starter')
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.trains', {
      url: '/trains',
      views: {
        'menuContent': {
          templateUrl: 'templates/trains/index.html',
          controller: 'TrainsCtrl'
        }
      }
    })

    .state('app.trains-add', {
      url: '/trains/add',
      views: {
        'menuContent': {
          templateUrl: 'templates/trains/add.html',
          controller: 'TrainsNewCtrl'
        }
      }
    })

  .state('app.train-show', {
    url: '/trains/:trainId',
    views: {
      'menuContent': {
        templateUrl: 'templates/trains/show.html',
        controller: 'TrainsShowCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/trains');
});