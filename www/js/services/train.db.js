'use strict';
angular.module("starter")
  .factory('TrainDB', function($q, $cordovaSQLite) {
    var openDB = function(){
      var db = window.cordova ? $cordovaSQLite.openDB({name: "unc001.db", location:'default'}) : window.openDatabase("unc001.db", "1.0", "UncDB", -1);
      return db;
    }

    var init = function(){
      var db = openDB();
      $cordovaSQLite.execute(
        db, 
        "CREATE TABLE IF NOT EXISTS trains " +
        "( id                   integer primary key   , " +
        "  name                 text       , " +
        "  number               text       , " +
        "  state                text       ) ")
        .then(function(response){ 
          console.log("Initialized table trains desde servicio");
          console.log(response);
        }), function(error){ 
          console.log("Error in initialized table trains");
        };
    }

    var create = function(trainParams){
      console.log("create()");
      var deferred = $q.defer();
      var db = openDB();

      var query = "INSERT INTO trains (name, number, state) VALUES (?, ?, ?)";
      // Execute query statement from query variable.
       $cordovaSQLite.execute(db, query, [trainParams.name, trainParams.number, trainParams.state]).then(function (res)
        {
          if(angular.isDefined(res.insertId)){
            trainParams.id = res.insertId;
            deferred.resolve(trainParams);
          }else{
            deferred.reject({ message: "No se inserto el dato." });
          }
        }, function(error){
          deferred.reject(error);
        });

      return deferred.promise;
    };
    var update = function(){

    };
    var destroy = function(){

    };
    var get = function(){
      
    };

    var all = function(){
      console.log("all()");
      var deferred = $q.defer();
      var db = openDB();
      var query = "SELECT * FROM trains";
      // Execute query statement from query variable.
      $cordovaSQLite.execute(db, query).then(function (res)
        {
          var records = [];
          if (res.rows.length > 0)
          {
            for (var i = 0; i < res.rows.length; i++)
            {
              var row = res.rows.item(i);
              
              var record = {
                id: row.id,
                name: row.name,
                number: row.number,
                state: row.state
              };
              
              records.push(record);  
            }
          }
          deferred.resolve(records); 
        }, function(error){
          deferred.reject(error);
        });     
      return deferred.promise;
    };

    return {
      init: init,
      create: create,
      update: update,
      destroy: destroy,
      get: get,
      all: all      
    }
  })