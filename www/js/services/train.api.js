'use strict';
angular.module("starter")
.factory('TrainApi', function($resource) {    
    var route = 'https://api-trains.herokuapp.com/trains/:id.json'
    var resource = $resource(route, {
      id:'@id',
      trains: '@params'
      }, 
      {
      });
    return resource;
  })